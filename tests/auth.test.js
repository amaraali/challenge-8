const request = require("supertest");
const app = require("../app");
const { User } = require("../app/models");

afterAll(async () => {
    await User.destroy({
        where: {
            email: "amara@mail.com",
        },
    });
});

// ==============LOGIN==============

describe("/POST /v1/auth/login", () => {
    it("should return 201 as status code", async () => {
        return request(app)
            .post("/v1/auth/login")
            .send({
                email: "ali@mail.com",
                password: "123456",
            })
            .then((res) => {
                expect(res.status).toBe(201);
                expect(res.body.accessToken).toBeDefined();
            }
            );
    });
    it("should return 404 Not Found if the user is not found", async () => {
        return request(app)
            .post("/v1/auth/login")
            .send({
                email: "haha@mail.com",
                password: "123456",
            })
            .then((res) => {
                expect(res.status).toBe(404);
                expect(res.body.error).toBeDefined();
            }
            );
    });
    it("should return 401 Wrong Password", async () => {
        return request(app)
            .post("/v1/auth/login")
            .send({
                email: "ali@mail.com",
                password: "1234567890",
            })
            .then((res) => {
                expect(res.status).toBe(401);
                expect(res.body).toBeDefined();
            }
            );
    });
});

// =============REGISTER===============

describe("/POST /v1/auth/register", () => {
    it("should return 422 email is already used", async () => {
        return request(app)
            .post("/v1/auth/register")
            .send({
                name: "Ali",
                email: "ali@mail.com",
                password: "123456",
            })
            .then((res) => {
                expect(res.status).toBe(422);
                expect(res.body.error).toBeDefined();
            }
            );
    });
    it("should return 201 register success", async () => {
        return request(app)
            .post("/v1/auth/register")
            .set("Content-Type", "application/json")
            .send({
                name: "Amara",
                email: "amara@mail.com",
                password: "123456",
            })
            .then((res) => {
                expect(res.status).toBe(201);
                expect(res.body.accessToken).toBeDefined();
            }
            );
    });
    it
});

// ==============WHOAMI===============

describe("/GET /v1/auth/whoami", () => {
    it("should return 200 as status code", async () => {
        const accessToken = await request(app).post("/v1/auth/login").send({
            email: "brian@mail.com",
            password: "123456",
        });

        return request(app)
            .get("/v1/auth/whoami")
            .set(
                "Authorization",
                `Bearer ${accessToken.body.accessToken}`)
            .then((res) => {
                expect(res.status).toBe(200);
                expect(res.body).toBeDefined();
            }
            );
    });
    it("should return 401 Unauthorized", async () => {
        return request(app)
            .get("/v1/auth/whoami")
            .set("Authorization", "Bearer uvbhvjshbfvjHBHBOBUOubobOBIOBIhbihBOIBiobIBO8YGOYHBNLhvfiuy")
            .then((res) => {
                expect(res.status).toBe(401);
                expect(res.body).toBeDefined();
            }
            );
    }
    );
});